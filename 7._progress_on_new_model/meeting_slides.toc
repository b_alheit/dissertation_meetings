\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.36pt}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{New Strain Energy Function}{2}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{1}{The SEF}{2}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{2}{LH Condition}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{3}{The Cauchy Stress}{4}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{4}{Verification}{5}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{FEM Model for Mechanical Properties}{7}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{The Model}{7}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{2}{Approximating the Tangent Matrix}{8}{0}{2}
