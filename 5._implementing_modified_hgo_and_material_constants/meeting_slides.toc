\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.36pt}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Modified HGO UMAT Validation}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{Exact Vs Abaqus Stress Stretch Curve}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Soft Collagenous Tissue Material properties}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{The Problem}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{2}{Collagenous Rat Tail Tendon}{5}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{3}{Soft Human Tissue}{6}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{4}{In Vitro Collagen Tissue Generation}{7}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{5}{Atomistic modelling simulation of collagen fibres}{8}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{6}{Anisotropic human skin tensile tests}{9}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{7}{Proposed solution}{10}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{References}{11}{0}{3}
