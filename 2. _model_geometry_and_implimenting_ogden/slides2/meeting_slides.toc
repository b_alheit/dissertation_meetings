\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Overview of what I have been doing}{4}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Model Geometry}{5}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{Geometry design}{5}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {3}{1}{1}{High level assembly}{5}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {3}{1}{2}{Bone design}{6}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {3}{1}{3}{Collagen design}{9}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {3}{1}{4}{Assembly of bone and collagen}{11}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {3}{1}{5}{Keratin?}{12}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{2}{Python script to control everything}{13}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{3}{Suture interface}{14}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Implementing Ogden model in a UMAT}{22}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{1}{Motivation}{22}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{2}{UMAT interface}{23}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{3}{Determining the tangent matrix i.t.o. Cauchy stress and strain}{24}{0}{3}
