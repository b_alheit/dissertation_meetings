\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.36pt}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Overview of what I have been doing}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Model Geometry}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{Geometry design}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {3}{1}{1}{High level assembly}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {3}{1}{2}{Bone design}{5}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {3}{1}{3}{Collagen design}{8}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {3}{1}{4}{Assembly of bone and collagen}{10}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {3}{1}{5}{Keratin?}{11}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{2}{Python script to control everything}{12}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{3}{Suture interface}{13}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Implementing Ogden model in a UMAT}{21}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {4}{0}{1}{Motivation}{21}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {4}{0}{2}{UMAT interface}{22}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsubsectionintoc {4}{0}{3}{Strain energy function for the incompressible Ogden model}{23}{0}{3}
