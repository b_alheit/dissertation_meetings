\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Improving model geometry}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{Concerns and to-do's from last meeting}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{2}{Improving tooth geometry}{4}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{3}{Interface between suture and top face}{8}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{4}{Keratin Layer}{10}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{5}{All together}{11}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Preliminary simulation}{12}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{Meshing}{13}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{2}{Linear Elastic Test Simulation}{15}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{UMATs}{16}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{1}{Approximating the Tangent Modulus}{16}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{2}{Near incompressible Neo-Hookean as a test}{21}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{3}{Adding anisotropy}{22}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{4}{Implementation in Abaqus}{25}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{5}{Changing isochoric component to Ogden Model}{29}{0}{3}
