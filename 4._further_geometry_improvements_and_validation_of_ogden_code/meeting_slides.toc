\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Final geometry improvements}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Modelling considerations}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{Spring boundary condition}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Coding the Ogden model}{6}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{1}{Validation with results from literature - simple tension}{6}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{2}{Validation with results from literature - equi-biaxial tension}{10}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{Consistent Tangent Matrices}{11}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{1}{Consistent Tangent Matrix of Volumetric Stress Component}{12}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{2}{Consistent Tangent Matrix of Isochoric Anisotropic Stress Component}{13}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{3}{Consistent Tangent Matrix of Anisotropic Stress Component}{14}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {6}{Material Properties}{15}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {7}{References}{19}{0}{6}
